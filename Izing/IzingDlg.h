﻿
// IzingDlg.h: файл заголовка
//

#pragma once
#include <vector>
using namespace std;

class Energy
{
public:
	double energy = 0.0;
	double pow_energy = 0.0;
	bool is_need_calc = false;
};


// Диалоговое окно CIzingDlg
class CIzingDlg : public CDialogEx
{
// Создание
public:
	CIzingDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IZING_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	int N;
	double T;
	double J;
	int shag;
	CProgressCtrl kontrolShag;
	CButton Mult;
	CButton Rachet;




	vector<vector<int>> vecIzingModel;
	int RandStaff(int min, int max);
	vector<int> BorderConditions(int size, int rand_idx);
	//double CalculateHamiltonian(int i, int j, int n_i, int n_j, vector<vector<int>> new_cfg);
	double CalculateHamiltonian(vector<vector<int>> last_cfg, vector<vector<int>> new_cfg);
	void MonteCarloStep(vector<vector<int>>& configuration, int step_count);
	void MonteCarlo();

	DWORD dwThread;
	HANDLE hThread;
	BOOL bRunTh = false;

	DWORD dwThreadGraphs;
	HANDLE hThreadGraphs;
	BOOL bRunThGraph = false;

	double k = 1.36e-23;
	void Draw2D(CRect& Pic, CDC* PicDc, std::vector<std::vector<int>> WV);


	CPen* pen;
	CPen setka_pen, osi_pen;
	CPen graph1pen;
	CPen graf_pen2;
	CPen graf_pen3;
	CFont fontgraph;

	double xminget, xmaxget,
		yminget, ymaxget,
		xpget, ypget;


	CWnd* PicWnd_ConfigurSystem;
	CDC* PicDc_ConfigurSystem;
	CRect Pic_ConfigurSystem;

	afx_msg void OnBnClickedNachkonf();
	void DrawImage(vector<vector<int>> vec, CDC* WinDc, CRect WinxmaxGraphc);

	double xpImage = 0, ypImage = 0,			//коэфициенты пересчета
		xminImage = -1, xmaxImage = 1,			//максисимальное и минимальное значение х 
		yminImage = -0.5, ymaxImage = 5;			//максисимальное и минимальное значение y
	afx_msg void OnBnClickedDo();
	vector<double> E;
	afx_msg void OnBnClickedresaerch();
	double srE = 0;
	double d;
	double S1;
	double S2;
	void CalculateGraphs();
	vector<vector<int>> GenerateConfiguration(int size);
	double Proshagaly;
};
