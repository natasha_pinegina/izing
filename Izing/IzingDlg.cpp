﻿
// IzingDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Izing.h"
#include "IzingDlg.h"
#include "afxdialogex.h"
#include <fstream>
#include <random>
#include <iterator>
#include <math.h>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DOTSIMAGE(x,y) (xpImage*((x)-xminImage)),(ypImage*((y)-ymaxImage))

// Диалоговое окно CIzingDlg

DWORD WINAPI MyProc(PVOID pv)
{
	CIzingDlg* p = (CIzingDlg*)pv;
	p->MonteCarlo();
	return 0;
}

DWORD WINAPI CalcGraph(PVOID pv)
{
	CIzingDlg* p = (CIzingDlg*)pv;
	p->CalculateGraphs();
	return 0;
}

CIzingDlg::CIzingDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IZING_DIALOG, pParent)
	, N(20)
	, T(0.5)
	, J(1)
	, shag(510)
	, d(0.5)
	, S1(0)
	, S2(0)
	,Proshagaly(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIzingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_N, N);
	DDX_Text(pDX, IDC_T, T);
	DDX_Text(pDX, IDC_J, J);
	DDX_Text(pDX, IDC_shag, shag);
	DDX_Control(pDX, IDC_PROGRESS1, kontrolShag);
	DDX_Control(pDX, IDC_Mult, Mult);
	DDX_Control(pDX, IDC_Rachet, Rachet);
	DDX_Text(pDX, IDC_d, d);
	DDX_Text(pDX, IDC_S1, S1);
	DDX_Text(pDX, IDC_S2, S2);
	DDX_Text(pDX, IDC_Proshagaly, Proshagaly);

}

BEGIN_MESSAGE_MAP(CIzingDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_NachKonf, &CIzingDlg::OnBnClickedNachkonf)
	ON_BN_CLICKED(IDC_Do, &CIzingDlg::OnBnClickedDo)
	ON_BN_CLICKED(IDC_resaerch, &CIzingDlg::OnBnClickedresaerch)
END_MESSAGE_MAP()


// Обработчики сообщений CIzingDlg

BOOL CIzingDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	srand((unsigned int)time(0));

	PicWnd_ConfigurSystem = GetDlgItem(IDC_Spin);
	PicDc_ConfigurSystem = PicWnd_ConfigurSystem->GetDC();
	PicWnd_ConfigurSystem->GetClientRect(&Pic_ConfigurSystem);

	kontrolShag.SetScrollRange(0., (double)shag, TRUE);

	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_Mult));
	pcb1->SetCheck(1);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CIzingDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CIzingDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CIzingDlg::DrawImage(vector<vector<int>> vec, CDC* WinDc, CRect WinxmaxGraphc)
{
		double GU_X = N + 2;
		double GU_Y = N + 2;

		xminImage = -GU_X * 0.01;
		xmaxImage = GU_X * 1.01;
		yminImage = -GU_Y * 0.01;			//минимальное значение y
		ymaxImage = GU_Y * 1.01;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpImage = (widthX / (xmaxImage - xminImage));			//Коэффициенты пересчёта координат по Х
	ypImage = -(heightY / (ymaxImage - yminImage));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(240, 240, 240));

	CPen line_pen;
	line_pen.CreatePen(		//для сетки
		PS_SOLID,					//пунктирная
		3,						//толщина 1 пиксель
		RGB(255, 0, 0));			//цвет  grey

	CBrush white_circle(RGB(139, 0, 0));			//цвет white
	CBrush black_circle(RGB(255, 215, 0));			//цвет black

	if (!vec.empty()) {
		vector<int> vecHelpMinLeft;
		vector<int> vecHelpMaxRight;
		vector<int> vecHelpMinTop;
		vector<int> vecHelpMaxBotomm;

		vector<vector<int>> IzingModel;
		int center = vec.size() / 2;
		// СЕЛЕДКА
		
			vector<int> vecHelp1(N + 2, 0);
			vector<vector<int>> vecHelp2(N + 2, vecHelp1);
			IzingModel = vecHelp2;

			
				for (int i = 0; i < vec.size(); i++)
				{
					IzingModel[i + 1][0] = vec[i][vec.size() - 1];
					IzingModel[i + 1][IzingModel.size() - 1] = vec[i][0];

					for (int j = 0; j < vec[i].size(); j++)
					{
						IzingModel[0][j + 1] = vec[vec.size() - 1][j];
						IzingModel[IzingModel.size() - 1][j + 1] = vec[0][j];

						IzingModel[i + 1][j + 1] = vec[i][j];
					}
				}
			

		// РЫБА
		
			for (int i = 0; i < IzingModel.size(); i++)
			{
				for (int j = 0; j < IzingModel[i].size(); j++)
				{
					double xxi = i;
					double yyi = j;
					int color = IzingModel[i][j];

					if (color == -1)
					{
						MemDc->MoveTo(DOTSIMAGE(xxi, yyi));
						CRect rect(DOTSIMAGE(xxi, yyi + 1), DOTSIMAGE(xxi + 1, yyi));
						LPCRECT lpRect = rect;
						MemDc->FillRect(lpRect, &white_circle);
					}
					else if (color == 1)
					{
						MemDc->MoveTo(DOTSIMAGE(xxi, yyi));
						CRect rect(DOTSIMAGE(xxi, yyi + 1), DOTSIMAGE(xxi + 1, yyi));
						LPCRECT lpRect = rect;
						MemDc->FillRect(lpRect, &black_circle);
					}
				}
			}


			for (int i = 0; i < IzingModel.size(); i++)
			{
				for (int j = 0; j < IzingModel[i].size(); j++)
				{
					double xxi = i;
					double yyi = j;
		
						MemDc->MoveTo(DOTSIMAGE(xxi, 0));
						MemDc->LineTo(DOTSIMAGE(xxi, yyi));

						MemDc->MoveTo(DOTSIMAGE(0, yyi));
						MemDc->LineTo(DOTSIMAGE(xxi, yyi));
				}
			}
		
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}


int CIzingDlg::RandStaff(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	// Равномерно распределяем рандомное число в нашем диапазоне
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

vector<vector<int>> CIzingDlg::GenerateConfiguration(int size) 
{
	// Инициализация 3-х мерного вектора значением -1 (сорт А).
	vector<int> vecHelp1(size, -1);
	vector<vector<int>> IzingModel(size, vecHelp1);
	//vector<vector<vector<int>>> IzingModel(size, vecHelp2);

	// Расчет количества элементов сорта Б.
	int cell_size = (size * size) / 2;
	int c = 0;
	while (c < cell_size)
	{
		// Рандомно выбираем место в векторе.
		int i = RandStaff(0, size - 1);
		int j = RandStaff(0, size - 1);
		//int k = RandStaff(0, size - 1);

		// Меняем сорт А на сорт Б, пока кол-во А != колву Б.
		if (IzingModel[i][j] == -1)
		{
			IzingModel[i][j] = 1;
			c++;
		}
	}
	return IzingModel;
}

void CIzingDlg::OnBnClickedNachkonf()
{
	UpdateData(TRUE);

	// Заполнение глобального вектора.
	vecIzingModel = GenerateConfiguration(N);

	//Draw2D(Pic_ConfigurSystem, PicDc_ConfigurSystem, vecIzingModel);
	DrawImage(vecIzingModel, PicDc_ConfigurSystem, Pic_ConfigurSystem);
}


template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
	std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
	std::advance(start, dis(g));
	return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
	static std::random_device rd;
	static std::mt19937 gen(rd());
	return select_randomly(start, end, gen);
}

vector<int> CIzingDlg::BorderConditions(int size, int rand_idx) {
	// Для получения всех соседей.
	vector<int> neigbours;
	// Если выбран не граничный атом.
	if (rand_idx > 0 && rand_idx < size - 1) {
		neigbours.push_back(rand_idx - 1);
		neigbours.push_back(rand_idx + 1);
	}
	// Если выбран атом на левой границе.
	else if (rand_idx == 0) {
		neigbours.push_back(size - 1);
		neigbours.push_back(rand_idx + 1);
	}
	// Если выбран атом на правой границе.
	else if (rand_idx == size - 1) {
		neigbours.push_back(rand_idx - 1);
		neigbours.push_back(0);
	}
	return neigbours;
}

double CIzingDlg::CalculateHamiltonian(vector<vector<int>> last_cfg, vector<vector<int>> new_cfg)
{
	if (!new_cfg.empty() && !last_cfg.empty()) {
		// Расчет энергии для старой конфигурации.
		double last_energy = 0.0;
		double new_energy = 0.0;
		for (int i = 1; i < last_cfg.size(); i++) {
			for (int j = 1; j < last_cfg[i].size(); j++) {
					last_energy += last_cfg[i][j] * (last_cfg[i - 1][j] + last_cfg[i][j - 1] + last_cfg[i][j]);
					new_energy += new_cfg[i][j] * (new_cfg[i - 1][j] + new_cfg[i][j - 1] + new_cfg[i][j]);
			}
		}
		last_energy *= -J;
		new_energy *= -J;

		return new_energy - last_energy;
	}
}

/**
Выполнение одного изменения конфигурации.
**/
void CIzingDlg::MonteCarloStep(vector<vector<int>>& configuration, int step_count) 
{
	// Проверка, что начальная конфигурация существует.
	if (!configuration.empty()) {
		int current_step = 0;
		// 1 МКШ.
		while (current_step < step_count) {
			// Выбор случайного спина.
			int rand_i = RandStaff(0, configuration.size() - 1);
			int rand_j = RandStaff(0, configuration[0].size() - 1);
			//int rand_k = RandStaff(0, configuration[0][0].size() - 1);

			// Индексы соседнего спина.
			int neig_i = rand_i;
			int neig_j = rand_j;
			//int neig_k = rand_k;

			// Получение вектора соседей по случайной оси, индексы.
			vector<int> axis{ 0, 1, 2 };
			while (!axis.empty()) {
				int axis_idx = RandStaff(0, axis.size() - 1);
				// Обработка оси X.
				if (axis[axis_idx] == 0) {
					vector<int> neigbour_i = BorderConditions(configuration.size(), rand_i);
					for (int i = 0; i < neigbour_i.size(); i++) {
						// Проверка на разносортность.
						if (configuration[rand_i][rand_j] == configuration[neigbour_i[i]][neig_j]) {
							neigbour_i.erase(neigbour_i.begin() + i);
						}
					}

					// Проверка оставшихся соседей.
					if (!neigbour_i.empty()) {
						int random_idx = RandStaff(0, neigbour_i.size() - 1);
						neig_i = neigbour_i[random_idx];
						break;
					}
					else {
						axis.erase(axis.begin() + axis_idx);
					}
				}
				// Обработка оси Y.
				else if (axis[axis_idx] == 1) {
					vector<int> neigbour_j = BorderConditions(configuration[0].size(), rand_j);
					for (int i = 0; i < neigbour_j.size(); i++) {
						// Проверка на разносортность.
						if (configuration[rand_i][rand_j] == configuration[neig_i][neigbour_j[i]]) {
							neigbour_j.erase(neigbour_j.begin() + i);
						}
					}

					// Проверка оставшихся соседей.
					if (!neigbour_j.empty()) {
						int random_idx = rand() % neigbour_j.size();
						neig_j = neigbour_j[random_idx];
						break;
					}
					else {
						axis.erase(axis.begin() + axis_idx);
					}
				}
				
			}

			// Если нужный сосед найден - меняется спин, считаются энергии.
			if (rand_i != neig_i || rand_j != neig_j) {
				// Генерация новой конфигурации.
				vector<vector<int>> new_configuration = configuration;
				int temp = new_configuration[rand_i][rand_j];
				new_configuration[rand_i][rand_j] = new_configuration[neig_i][neig_j];
				new_configuration[neig_i][neig_j] = temp;

				// Расчет энергий, изменения энергии.
				double hamiltonian = CalculateHamiltonian(configuration, new_configuration);
				if (hamiltonian < 0) {
					configuration = new_configuration;
				}
				else {
					// Случайное число в диапазоне [0;1].
					double random_value = (double)(rand()) / RAND_MAX;
					double exponent = exp(-hamiltonian / (T));
					if (random_value < exponent) {
						configuration = new_configuration;
					}
				}
			}
			current_step++;
		}
	}
}

/**
Выполнение алгоритма метрополиса.
**/
void CIzingDlg::MonteCarlo() {
	int counter = 1;
	char step[100];
	while (true) {
		S1 = 0;
		S2 = 0;
		// Вывод иинформации на экран.
		//sprintf_s(step, "%d", counter);
		//CURRENT_MKSH_STEP.SetWindowTextW((CString)step);

		// Выполнение 1 Монте-Карло шага.
		int steps = N * N;
		MonteCarloStep(vecIzingModel, steps);
		if(Mult.GetCheck())
		DrawImage(vecIzingModel, PicDc_ConfigurSystem, Pic_ConfigurSystem);
		//Draw2D(Pic_ConfigurSystem, PicDc_ConfigurSystem, vecIzingModel);
		kontrolShag.SetPos(counter);
		counter++;

		Proshagaly = counter;

		for (int i = 0; i < vecIzingModel.size(); i++)
		{
			for (int j = 0; j < vecIzingModel[i].size(); j++)
			{
				if (vecIzingModel[i][j]==1) S1++;
				else S2++;
			}
		}
		UpdateData(false);

		// Условие завершения.
		if (counter >= shag) {
			break;
		}
		
	}
	//if(Rachet.GetCheck())
	//MessageBox(L"Визуализация успешно завершена!", L"Информация", MB_ICONINFORMATION | MB_OK);
	bRunTh = false;
	//button_picture.SetWindowTextW(bStartVisualization);
	TerminateThread(hThread, 0);
	CloseHandle(hThread);
	hThread = NULL;
	vecIzingModel.clear();
}

void CIzingDlg::OnBnClickedDo()
{
	// Выполнение визуализации МКШ.
	UpdateData(TRUE);
	if (!bRunTh) {
		if (hThread == NULL) {
			if (vecIzingModel.empty()) 
			{
				vecIzingModel.clear();
				OnBnClickedNachkonf();
			}

			hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
		}
		else {
			ResumeThread(hThread);
		}
		bRunTh = true;
	}
	else {
		//button_picture.SetWindowTextW(bStartVisualization);
		bRunTh = false;
		SuspendThread(hThread);
	}
}





void CIzingDlg::CalculateGraphs() 
{
	double T_CRITICAL = 2.2269;
	double start_temp = 0.1 * T_CRITICAL;
	double stop_temp = 2.0 * T_CRITICAL;
	double dots_count = 100;
	double step = (stop_temp - start_temp) / dots_count;

	int size = 60;
	int mksh_count = 1000;
	char current_step[100];
	char current_temp[100];

	// Энергия.
	Energy enrg;
	vector<double> y_energy;
	vector<double> y_capacity;
	vector<double> x;
	vector<vector<int>> conf = GenerateConfiguration(size);
	int THRESHOLD_MKSH = 500;
	for (double t = start_temp; t < stop_temp; t += step) {
		T = t;
		enrg.energy = 0.0;
		enrg.pow_energy = 0.0;
		for (int mksh = 0; mksh < mksh_count; mksh++) {
			// Вывод иинформации на экран.
			sprintf_s(current_step, "%d", mksh);
			//CURRENT_MKSH_STEP.SetWindowTextW((CString)current_step);

			MonteCarloStep(conf, size * size);

			if (mksh > THRESHOLD_MKSH) {
				double energy = 0.0;
				for (int i = 1; i < conf.size(); i++) {
					for (int j = 1; j < conf[i].size(); j++) {
					
							energy += conf[i][j] * (conf[i - 1][j] + conf[i][j - 1] + conf[i][j]);
						
					}
				}
				energy *= -J;
				enrg.energy += energy;
			}
			UpdateData(false);
		}

		// Усреднение.
		enrg.energy /= (mksh_count - THRESHOLD_MKSH);
		enrg.energy /= size * size	;

		ofstream out_x("energy_x.txt", ios_base::app);
		out_x << t << endl;
		out_x.close();

		ofstream out_y("energy_y.txt", ios_base::app);
		out_y << enrg.energy << endl;
		out_y.close();
	}

	MessageBox(L"Визуализация успешно завершена!", L"Информация", MB_ICONINFORMATION | MB_OK);
	bRunThGraph = false;
	//button_plots.SetWindowTextW(bStartVisualization);
	TerminateThread(hThreadGraphs, 0);
	CloseHandle(hThreadGraphs);
	hThreadGraphs = NULL;
}

void CIzingDlg::OnBnClickedresaerch()
{
	//E.clear();
	//ofstream inf("Energy.txt");
	//
	//double Tc = 2.269;
	//double shagg = (1.5 * Tc - 0.3 * Tc) / 50;
	//double Iterr = 0;
	//for (double t = 0.3 * Tc; t < 1.5 * Tc; t += shagg)
	//{
	//	srE = 0;
	//	T = t;
	//	UpdateData(false);
	//	int counter = 1;
	//	char step[100];
	//	while (true) 
	//	{
	//		// Выполнение 1 Монте-Карло шага.
	//		int steps = N * N;
	//		MonteCarloStep(vecIzingModel, steps);
	//		//Draw2D(Pic_ConfigurSystem, PicDc_ConfigurSystem, vecIzingModel);
	//		counter++;

	//		// Условие завершения.
	//		if (counter >= shag) {
	//			break;
	//		}
	//	}
	//	double kvE = (srE * srE) / (N * N);
	//	srE /= N * N;
	//	double C = (kvE- srE* srE) / (N * N * k * k * T * T);
	//	inf << srE <<"\t" <<C<< endl;
	//	/*GetEnergy.SetPos(Iterr);
	//	Iterr++;*/
	//	E.push_back(srE);
	//}
	////MessageBox::Show("Hello World");
	////MessageBox(NULL, NULL, "MyText");
	//inf.close();
	UpdateData(TRUE);
	if (!bRunThGraph) {
		//button_plots.SetWindowTextW(bStopVisualization);
		if (hThreadGraphs == NULL) {
			hThreadGraphs = CreateThread(NULL, 0, CalcGraph, this, 0, &dwThreadGraphs);
		}
		else {
			ResumeThread(hThreadGraphs);
		}
		bRunThGraph = true;
	}
	else {
		//button_plots.SetWindowTextW(bStartVisualization);
		bRunThGraph = false;
		SuspendThread(hThreadGraphs);
	}
}